import { newSpecPage } from '@stencil/core/testing';
import { MyComponent } from './my-component';

describe('my-component', () => {
  it('renders', async () => {
    const { root } = await newSpecPage({
      components: [MyComponent],
      html: '<my-component></my-component>',
    });
    expect(root).toEqualHtml(`
      <my-component>
        <gl-app label="Basic Application">
           <gl-fullscreen slot="end-buttons"></gl-fullscreen>
           <gl-basemaps slot="start-buttons"></gl-basemaps>
           <gl-map latitude="40.105387" longitude="-88.218767" maxzoom="22" zoom="13">
             <gl-style basemap="" enabled="" name="2023 Aerial Hybrid" thumbnail="https://maps.ccrpc.org/tiles/champaign_county_2023/14/4172/6197.jpg" url="https://maps.ccrpc.org/basemaps/hybrid-2023/style.json"></gl-style>
           </gl-map>
         </gl-app>
      </my-component>
    `);
  });

  it('renders with values', async () => {
    const { root } = await newSpecPage({
      components: [MyComponent],
      html: `<my-component></my-component>`,
    });
    expect(root).toEqualHtml(`
      <my-component>
        <gl-app label="Basic Application">
           <gl-fullscreen slot="end-buttons"></gl-fullscreen>
           <gl-basemaps slot="start-buttons"></gl-basemaps>
           <gl-map latitude="40.105387" longitude="-88.218767" maxzoom="22" zoom="13">
             <gl-style basemap="" enabled="" name="2023 Aerial Hybrid" thumbnail="https://maps.ccrpc.org/tiles/champaign_county_2023/14/4172/6197.jpg" url="https://maps.ccrpc.org/basemaps/hybrid-2023/style.json"></gl-style>
           </gl-map>
         </gl-app>
      </my-component>
    `);
  });
});
