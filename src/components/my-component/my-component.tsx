import { Component, Host, h } from '@stencil/core';

@Component({
  tag: 'my-component',
  styleUrl: 'my-component.css',
  shadow: false,
})
export class MyComponent {
  render() {
    return (
      <Host>
        <gl-app label="Basic Application" menu={false}>
          <gl-fullscreen slot="end-buttons"></gl-fullscreen>
          <gl-basemaps slot="start-buttons"></gl-basemaps>
          <gl-map
            // ref={(r: HTMLGlMapElement) => (this.map = r)}
            longitude={-88.218767}
            latitude={40.105387}
            zoom={13}
            maxzoom={22}
          >
            <gl-style
              url="https://maps.ccrpc.org/basemaps/hybrid-2023/style.json"
              basemap={true}
              thumbnail="https://maps.ccrpc.org/tiles/champaign_county_2023/14/4172/6197.jpg"
              name="2023 Aerial Hybrid"
              enabled={true}
            ></gl-style>
          </gl-map>
        </gl-app>
      </Host>
    );
  }
}
