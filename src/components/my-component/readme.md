# my-component



<!-- Auto Generated Below -->


## Dependencies

### Depends on

- gl-app
- gl-fullscreen
- gl-basemaps
- gl-map
- gl-style

### Graph
```mermaid
graph TD;
  my-component --> gl-app
  my-component --> gl-fullscreen
  my-component --> gl-basemaps
  my-component --> gl-map
  my-component --> gl-style
  gl-app --> ion-menu
  gl-app --> ion-header
  gl-app --> ion-toolbar
  gl-app --> ion-title
  gl-app --> ion-content
  gl-app --> ion-menu-toggle
  gl-app --> ion-button
  gl-app --> ion-icon
  gl-app --> ion-buttons
  gl-app --> ion-footer
  gl-app --> ion-split-pane
  gl-app --> ion-app
  ion-menu --> ion-backdrop
  ion-button --> ion-ripple-effect
  gl-fullscreen --> ion-button
  gl-fullscreen --> ion-icon
  gl-basemaps --> gl-basemap-switcher
  gl-basemaps --> ion-button
  gl-basemaps --> ion-icon
  gl-basemap-switcher --> ion-item
  gl-basemap-switcher --> ion-thumbnail
  gl-basemap-switcher --> ion-radio
  gl-basemap-switcher --> ion-label
  gl-basemap-switcher --> ion-content
  gl-basemap-switcher --> ion-list
  gl-basemap-switcher --> ion-radio-group
  gl-basemap-switcher --> ion-list-header
  ion-item --> ion-icon
  ion-item --> ion-ripple-effect
  ion-item --> ion-note
  style my-component fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
