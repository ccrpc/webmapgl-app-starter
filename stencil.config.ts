import { Config } from '@stencil/core';

export const config: Config = {
  namespace: 'webmapgl-app-starter',
  globalStyle: 'src/global/app.css',
  globalScript: 'src/global/app.ts',
  outputTargets: [
    {
      type: 'docs-readme',
    },
    {
      type: 'docs-vscode',
      file: 'vscode-data.json',
    },
    {
      type: 'www',
      serviceWorker: null, // disable service workers
    },
  ],
  testing: {
    browserHeadless: "new",
    browserArgs: ['--no-sandbox', '--disable-setuid-sandbox',
      '--enable-logging', '--v=1',
      '--remote-debugging-port=0',
      '--disable-extension']
  },
};
